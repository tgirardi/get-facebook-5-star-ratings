# get-facebook-5-star-ratings

Gets all positive recommendations (and 5-stars ratings, from the old facebook's rating system) for a given Facebook Page and stores it in a CSV file.

```
usage: get-facebook-5-star-ratings [-h] [-v] [-o OUTPUT_FILE] -t ACCESS_TOKEN
                                   -i FB_PAGE_ID


Creates a CSV file with all your facebook page's 5-stars ratings (old rating
system) and positive recommendations

Optional arguments:
  -h, --help            Show this help message and exit.
  -v, --version         Show program's version number and exit.
  -o OUTPUT_FILE, --output-file OUTPUT_FILE
                        path to the file that will store the resulting CSV
                        with all the "5-stars" ratings. Default = ./ratings.csv
  -t ACCESS_TOKEN, --access-token ACCESS_TOKEN
                        Facebook Access Token for the Facebook API request
                        that will get the list of ratings
  -i FB_PAGE_ID, --fb-page-id FB_PAGE_ID
                        Facebook Page ID of the page you want to get the
                        ratings from

```

As you can see, you must provide:

* A valid Facebook API **access token**. It has to be a Page Access Token with *manage_pages* permission.
* The Facebook Page **ID** (you can get that number at `https://www.facebook.com/pg/<your_facebook_page_name>/about`).

Optionally, you can indicate the path and file name, for the resulting CSV file (e.g. ~/Documents/my-page-ratings.csv).


For more information: [https://developers.facebook.com/docs/graph-api/reference/page/ratings/](https://developers.facebook.com/docs/graph-api/reference/page/ratings/).

## Why?

Because sometimes you need those "sweet" top reviews as social proof for marketing campaigns, sales processes, promotion, etc.

And having them on a CSV spreadsheet can be useful and a way of using that data in a comfortable way.
