#!/usr/bin/env node
'use strict';

const pack = require('./package.json');
const fs = require('fs');
const { convertArrayToCSV } = require('convert-array-to-csv');
const { getRatings, filterRatings } = require('fb-ratings');
const { ArgumentParser } = require('argparse');
const parser = new ArgumentParser({
  version: pack.version,
  addHelp:true,
  description: pack.description,
  prog: pack.name
});
parser.addArgument(
  [ '-o', '--output-file' ],
  {
    help: 'path to the file that will store the resulting CSV with all the "5-stars" ratings. Default = ./ratings.csv',
    defaultValue: './ratings.csv'
  }
);
parser.addArgument(
  [ '-t', '--access-token' ],
  {
    help: 'Facebook Access Token for the Facebook API request that will get the list of ratings',
    required: true
  }
);
parser.addArgument(
  [ '-i', '--fb-page-id' ],
  {
    help: 'Facebook Page ID of the page you want to get the ratings from',
    required: true
  }
);
parser.addArgument(
  [ '-s', '--std-out' ],
  {
    help: 'Print in stdout and not on file. Overrides --output-file',
    type: 'int',
    action: 'storeTrue',
  },
);
const args = parser.parseArgs();
const accessToken = args.access_token;
const fbPageId = args.fb_page_id;
const outputFile = args.output_file;
const stdOut = args.std_out;

getRatings(accessToken, fbPageId, []).then(ratings => {
  let csv = convertArrayToCSV(
    filterRatings(ratings)
      .map(({ rating, ...elem }) => elem)
      .map(elem => ({ ...elem, reviewer_name: elem.reviewer.name }))
      .map(({ reviewer, ...elem }) => elem)
      .map(({ open_graph_story, ...elem }) => elem)
    );
  if(stdOut) {
    console.log(csv);
  } else {
    fs.writeFile(outputFile, csv, err => {
          if (err) console.log(err);
          else console.log(`ratings saved as CSV in file ${outputFile}`);
    });
  }
}, error => console.log(error));
